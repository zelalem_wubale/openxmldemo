﻿using System.Collections.Generic;

namespace OpenXML.Excel.Export.Core
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> GetEmployees();
    }
}
