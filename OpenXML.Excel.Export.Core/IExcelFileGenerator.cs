﻿namespace OpenXML.Excel.Export.Core
{
    public interface IExcelFileGenerator
    {
        void GenerateExcelFile(string fileName);
    }
}
